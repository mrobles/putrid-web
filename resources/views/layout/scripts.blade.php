<!-- //// SCRIPTS //// -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/blazy.min.js"></script>
<script src="js/morphext.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/jquery-modal-video.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/strider.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB64kJJiSynOc9ZqkNMOyl94cvsw5Z2uno"></script>
