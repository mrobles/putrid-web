<head>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="author" content="mrobles">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- WEBSITE TITLE & DESCRIPTION -->
    <title>Putrid - The Game</title>
    <meta name="description" content="The best survivor game">
    <meta name="keywords" content="game, gaming, videogame">

    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>

    <!-- Favicon and Apple Icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">

    <!-- STYLES -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Animations -->
    <link href="css/animations.css" rel="stylesheet">
    <!-- Animation repository for morhpext -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Text Animator morphext -->
    <link href="css/morphext.css" rel="stylesheet">
    <!-- Lightbox -->
    <link href="css/lightbox.min.css" rel="stylesheet">
    <!-- Video Lightbox -->
    <link href="css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>