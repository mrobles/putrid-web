<!-- /// TEAM SECTION /// -->
<div id="team" class="large-margin">
    <a href="team.html"></a><!-- Nav Anchor -->
    <div class="row heading tiny-margin">
        <div class="col-md-auto">
            <h1 class="animation-element slide-down">THE <span class="colored">TEAM</span></h1>
        </div>
        <div class="col">
            <hr class="animation-element extend">
        </div>
    </div>
    <div class="row medium-margin">
        <div class="col-md-11 tiny-margin">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt
                mi ut mauris varius, vitae lobortis erat ullamcorper. Pellentesque vel dolor non nisi fringilla
                scelerisque in non ante.</p>
        </div>
        <div id="full-row" class="row text-center">
            <div class="col-md-3 team-card">
                <figure>
                    <img src="images/placeholder.jpg" data-src="images/test.png" class="img-fluid b-lazy"
                         alt="teammember">
                    <figcaption class="team-caption">
                        <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</p>
                        <hr class="hr-short">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <p class="team-name">TEST</p>
                <p class="subtle">Lead Designer</p>
            </div>
            <div class="col-md-3 team-card">
                <figure>
                    <img src="images/placeholder.jpg" data-src="images/test.png" class="img-fluid b-lazy"
                         alt="teammember">
                    <figcaption class="team-caption">
                        <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</p>
                        <hr class="hr-short">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <p class="team-name">TEST</p>
                <p class="subtle">Founder</p>
            </div>
            <div class="col-md-3 team-card">
                <figure>
                    <img src="images/placeholder.jpg" data-src="images/test.png" class="img-fluid b-lazy"
                         alt="teammember">
                    <figcaption class="team-caption">
                        <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</p>
                        <hr class="hr-short">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <p class="team-name">TEST</p>
                <p class="subtle">Lead Artist</p>
            </div>
            <div class="col-md-3 team-card">
                <figure>
                    <img src="images/placeholder.jpg" data-src="images/test.png" class="img-fluid b-lazy"
                         alt="teammember">
                    <figcaption class="team-caption">
                        <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</p>
                        <hr class="hr-short">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                        </ul>
                    </figcaption>
                </figure>
                <p class="team-name">TEST</p>
                <p class="subtle">Programmer</p>
            </div>
        </div>
    </div>
    <div class="row tiny-margin">
        <div class="col-md-11">
            <h2 class="short-hr-left">SCREENSHOT</h2>
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt, nisl non mattis--}}
                {{--sollicitudin, risus quam tempor sem, vel interdum est libero non odio.</p>--}}
        </div>
    </div>
    <div class="grid-gallery">
        <div class="row">
            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/sreyos-sol.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/sreyos-sol.png" data-src="images/Capturas/sreyos-sol.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>

            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/player-fuego.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/player-fuego.png" data-src="images/Capturas/player-fuego.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>

            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/player-playa.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/player-playa.png" data-src="images/Capturas/player-playa.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/builing.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/builing.png" data-src="images/Capturas/builing.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>
            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/sol.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/sol.png" data-src="images/Capturas/sol.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>
            <div class="col-md-4 gallery-item">
                <a href="images/Capturas/lago.png" data-lightbox="studio_gallery">
                    <div class="overlay gallery">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <img src="images/Capturas/lago.png" data-src="images/Capturas/lago.png" class="img-fluid b-lazy" alt="">
                </a>
            </div>

        </div>
    </div>
</div>