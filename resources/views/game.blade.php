<!-- /// GAMES SECTION /// -->
<div id="games" class="large-margin">
    <a href="games.html"></a><!-- Nav Anchor -->
    <div class="row heading tiny-margin">
        <div class="col-md-auto">
            <h1 class="animation-element slide-down">THE <span class="colored">GAME</span></h1>
        </div>
        <div class="col">
            <hr class="animation-element extend">
        </div>
    </div>
    <div class="row ">
        {{--<div class="col-md-11 small-margin">--}}
        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse--}}
        {{--facilisis rhoncus nibh. Phasellus dignissim luctus consectetur. Fusce viverra est non purus--}}
        {{--ultrices, vel molestie massa tincidunt.</p>--}}
        {{--</div>--}}
        <div class="col-md-12">
            <ul class="game-tags">
                <li>Sort By Tag :</li>
                <li><a href="#" data-filter=".new">NEW</a></li>
                <li><a href="#" data-filter=".game">GAME</a></li>
                <li><a href="#" data-filter=".object">OBJECT</a></li>
                <li><a href="#" data-filter=".build">BUILD</a></li>
            </ul>
        </div>
    </div>
    <div class="games-portfolio ">

        <!-- Game Card Video -->
        <div class="row game-card game new">
            <div class="col-lg-12 col-xl-5 game-card-left">
                <a href="#" class="js-video-button" data-video-id='bXqTUWnUJ20' data-channel="youtube">
                    <!-- Video link goes here -->
                    <div class="overlay">
                        <i class="fa fa-play fa-3x"></i>
                    </div>
                    <img src="images/Capturas/sreyos-sol.png" data-src="images/Capturas/sreyos-sol.png"
                         class="img-fluid b-lazy"
                         alt="video thumbnail"> <!-- Video Thumbnail Img -->
                </a>
            </div>
            <div class="col-lg-12 col-xl-7 game-card-right">
                <h2 class="short-hr-left">SANDBOX</h2>
                <p class="tags"><span class="subtle">Terrain generation</span></p>
                <p class="game-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                    facilisis rhoncus nibh. Phasellus dignissim luctus consectetur. Fusce viverra est non purus
                    ultrices, vel molestie massa tincidunt. <span class="expand colored strong" data-toggle="modal"
                                                                  data-target="#game2">Read More</span></p>
                <a href="#" class="button-store">
                    <i class="fa fa-wifi fa-2x"></i>
                    <p>Available on<br><span class="strong"> OTHER WEB</span></p>
                </a>
                <a href="#" class="button-store">
                    <i class="fa fa-windows fa-2x"></i>
                    <p>Available <br><span class="strong">HERE</span></p>
                </a>
                <div class="rating">
                    <p class="strong">4.5</p>
                    <ul>
                        <li><i class="fa fa-star colored"></i></li>
                        <li><i class="fa fa-star colored"></i></li>
                        <li><i class="fa fa-star colored"></i></li>
                        <li><i class="fa fa-star colored"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                    </ul>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade game-modal" id="game2" tabindex="-1" role="dialog" aria-labelledby="bedlam"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="bedlam">SANDBOX</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2 class="short-hr-left">Day cycle</h2>
                            <p>Full day cycle</p>
                            <img class="img-fluid" src="images/Capturas/sol.png" alt="screenshot">
                            <br>
                            <img class="img-fluid" src="images/Capturas/luna.png" alt="screenshot">
                            <br>
                            <img class="img-fluid" src="images/Capturas/Lluvia.png" alt="screenshot">
                            <br>
                            <img class="img-fluid" src="images/Capturas/lago.png" alt="screenshot">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="button secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Game Card End -->

        <!-- Game Card Video -->
        <div class="row game-card game build new">
            <div class="col-lg-12 col-xl-5 game-card-left">
                <a href="#" class="js-video-button" data-video-id='QVH9ylMkLic' data-channel="youtube">
                    <!-- Video link goes here  -->
                    <div class="overlay">
                        <i class="fa fa-play fa-3x"></i>
                    </div>
                    <img src="images/Capturas/plantas.png" data-src="images/Capturas/plantas.png"
                         class="img-fluid b-lazy"
                         alt="video thumbnail"> <!-- Video Thumbnail Img  -->
                </a>
            </div>
            <div class="col-lg-12 col-xl-7 game-card-right">
                <h2 class="short-hr-left">FARMING</h2>
                <p class="tags"><span class="subtle">Video</span></p>
                <p class="game-description">
                    Grow your own farm. Plant vegetables and fruits, harvest crops, employ workers, take care of
                    animals.
                    Buy farm equipment and relax!
            </div>

        </div><!-- Game Card End -->


        <!-- Game Card -->
        <div class="row game-card object new">
            <div class="col-lg-12 col-xl-5 game-card-left">
                <a href="images/Capturas/botellas.png" data-lightbox="screenshots_dark">
                    <div class="overlay">
                        <i class="fa fa-picture-o fa-3x"></i>
                    </div>
                    <picture>
                        <source media="(min-width: 1200px)" srcset="images/Capturas/botellas.png">
                        <source media="(min-width: 768px)" srcset="images/Capturas/botellas.png">
                        <img src="images/Capturas/botellas.png" data-src="images/Capturas/construccion.png" class="img-fluid b-lazy"
                             alt="aurora image">
                    </picture>
                </a>
                <a href="images/Capturas/construccion.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/Sierra.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/bici.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/sacos.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/botellas.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/cerillas.png" data-lightbox="screenshots_dark"></a>
                <a href="images/Capturas/carretilla.png" data-lightbox="screenshots_dark"></a>
            </div>
            <div class="col-lg-12 col-xl-7 game-card-right">
                <h2 class="short-hr-left">Objects</h2>
                <p class="tags"><span class="subtle">Full inventory system</span></p>
                <p class="game-description">
                    You will have an endless list of objects and constructibles available to interact with them
                    to do everything you can imagine
                </p>

            </div>
        </div><!-- Game Card End -->




        {{--<!-- Game Card -->--}}
        {{--<div class="row game-card pc">--}}
            {{--<div class="col-lg-12 col-xl-5 game-card-left">--}}
                {{--<a href="images/test.png" data-lightbox="screenshots_aurora">--}}

                    {{--<div class="overlay">--}}
                        {{--<i class="fa fa-picture-o fa-3x"></i>--}}
                    {{--</div>--}}
                    {{--<picture>--}}
                        {{--<source media="(min-width: 1200px)" srcset="images/test.png">--}}
                        {{--<source media="(min-width: 768px)" srcset="images/test.png">--}}
                        {{--<img src="images/placeholder.jpg" data-src="images/test.png" class="img-fluid b-lazy"--}}
                             {{--alt="aurora image">--}}
                    {{--</picture>--}}
                {{--</a>--}}
                {{--<a href="images/slider.png" data-lightbox="screenshots_aurora"></a>--}}
            {{--</div>--}}
            {{--<div class="col-lg-12 col-xl-7 game-card-right">--}}
                {{--<h2 class="short-hr-left">TEST</h2>--}}
                {{--<p class="tags"><span class="subtle">Action RPG | PC</span></p>--}}
                {{--<p class="game-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse--}}
                    {{--facilisis rhoncus nibh. Phasellus dignissim luctus consectetur. Fusce viverra est non purus--}}
                    {{--ultrices, vel molestie massa tincidunt. <span class="expand colored strong" data-toggle="modal"--}}
                                                                  {{--data-target="#game3">Read More</span></p>--}}
                {{--<div class="steam-btn">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-steam fa-3x"></i>--}}
                        {{--<p>GET IT ON <br><span class="spaced">STEAL</span></p>--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="reviews">--}}
                    {{--<a href="#">--}}
                        {{--<div class="score-card">--}}
                            {{--<p class="score">8.5</p>--}}
                            {{--<p>Gamespot</p>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="#">--}}
                        {{--<div class="score-card">--}}
                            {{--<p class="score">8.1</p>--}}
                            {{--<p>IGN</p>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="#">--}}
                        {{--<div class="score-card">--}}
                            {{--<p class="score">83</p>--}}
                            {{--<p>Metacritic</p>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Modal -->--}}
            {{--<div class="modal fade game-modal" id="game3" tabindex="-1" role="dialog" aria-labelledby="aurora"--}}
                 {{--aria-hidden="true">--}}
                {{--<div class="modal-dialog modal-lg" role="document">--}}
                    {{--<div class="modal-content">--}}
                        {{--<div class="modal-header">--}}
                            {{--<h1 class="modal-title" id="aurora">DESCRIPTION TEST</h1>--}}
                            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                {{--<span aria-hidden="true">&times;</span>--}}
                            {{--</button>--}}
                        {{--</div>--}}
                        {{--<div class="modal-body">--}}
                            {{--<iframe class="modal-vid"--}}
                                    {{--src="https://www.youtube.com/embed/K50-iBe7FpQ?rel=0&amp;showinfo=0"--}}
                                    {{--allowfullscreen></iframe>--}}
                            {{--<h2 class="short-hr-left">WORLD</h2>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget--}}
                                {{--pretium orci. Sed vestibulum rutrum volutpat. Curabitur feugiat arcu odio, quis--}}
                                {{--convallis eros laoreet ac.</p>--}}
                            {{--<img class="img-fluid" src="images/test.png" alt="screenshot">--}}
                            {{--<br>--}}
                            {{--<h2 class="short-hr-left">CHARACTER</h2>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, ipsum sem mattis diam, in--}}
                                {{--egestas--}}
                                {{--arcu lacus sed lectus. Donec at interdum tellus. Quisque pellentesque a felis et--}}
                                {{--rutrum. Donec condimentum magna sit amet viverra convallis. Fusce accumsan efficitur--}}
                                {{--orci a commodo.</p>--}}
                            {{--<img class="img-fluid" src="images/test.png" alt="screenshot">--}}
                            {{--<br>--}}
                            {{--<h2 class="short-hr-left">TRAIN</h2>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum--}}
                                {{--ultrices eleifend enim, quis maximus nibh dapibus in. Phasellus lacinia nec leo at--}}
                                {{--semper. Duis nisl odio, lacinia quis dui at, pretium tincidunt metus. Maecenas--}}
                                {{--condimentum purus sit amet neque maximus tempor. Proin eros massa, ullamcorper eget--}}
                                {{--rutrum eu, feugiat id purus.</p>--}}
                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="button secondary" data-dismiss="modal">Close</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div><!-- Game Card End -->--}}

    </div>
</div>