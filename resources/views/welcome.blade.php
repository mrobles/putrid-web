<!DOCTYPE html>
<html lang="en">

@include('layout.header')

<body>
<!-- Loading Screen -->
<div id="loader-wrapper">
    <h1 class="loader-logo"><span class="colored">P</span>UTRID</h1>
    <div id="progress"></div>
    <h3 class="loader-text">LOADING</h3>
</div>

<!-- //// HEADER //// -->
<header id="main-header">
    @include('layout.menu')
</header><!-- Header End -->

@include('main')

<!-- /// Main Container /// -->
<div class="container">

@include('about')
@include('game')
@include('team')

</div><!-- Main Container End -->


@include('layout.footer')

@include('layout.scripts')
</body>

</html>