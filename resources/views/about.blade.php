<!-- /// ABOUT SECTION /// -->
<div id="about" class="large-margin">
    <a href="about.html"></a><!-- Nav Anchor -->
    <div class="row heading tiny-margin">
        <div class="col-md-auto">
            <h1 class="animation-element slide-down">ABOUT
                <!--<span class="colored">US</span>-->
            </h1>
        </div>
        <div class="col">
            <hr class="animation-element extend">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p class="small-margin">
                Do whatever it takes to survive.
                Build a fire, build a shelter, kill enemys and protect yourself.
                Create alliances with other players and form a town.
            </p>
            <p class="small-margin">
                What happened? Who are you and where are you? - Unknown. You only know that you are not alone here.
                Do you want to survive? - A few have managed to survive with their bare hands. Arm yourself and set
                your way, it is the main issue today
            </p>
            {{--<img id="awards" src="images/awards.png" class="img-fluid" alt="awads">--}}
        </div>
        <div class="col-md-6">
            <img id="support-image" src="images/Capturas/Gif_Putrid.gif" data-src="images/Capturas/Gif_Putrid.gif"
                 class="img-fluid b-lazy" alt="digital collage">
        </div>
    </div>
</div>