<!-- /// CAREERS SECTION /// -->
<!--<div id="careers" class='large-margin'>-->
<!--<a href="careers.html"></a>&lt;!&ndash; Nav Anchor &ndash;&gt;-->
<!--<div class="row heading tiny-margin">-->
<!--<div class="col-md-auto">-->
<!--<h1 class="animation-element slide-down">WE'RE <span class="colored">HIRING</span></h1>-->
<!--</div>-->
<!--<div class="col">-->
<!--<hr class="animation-element extend">-->
<!--</div>-->
<!--</div>-->
<!--<div class="row medium-margin">-->
<!--<div class="col-md-11">-->
<!--<h2 class="short-hr-left">JOB OPENINGS</h2>-->
<!--<p>Are you a talented and motivated individual? Then we would love to have you in our team. Lorem ipsum-->
<!--dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt, nisl non mattis sollicitudin,-->
<!--risus quam tempor sem, vel interdum est libero non odio.</p><br>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="job-card">-->
<!--<h3 class="colored">LEAD PROGRAMMER</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
<!--<button class="button" data-toggle="modal" data-target="#modal1">View Details</button>-->
<!--</div>-->
<!--&lt;!&ndash; Modal &ndash;&gt;-->
<!--<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="lead-programmer"-->
<!--aria-hidden="true">-->
<!--<div class="modal-dialog modal-lg" role="document">-->
<!--<div class="modal-content">-->
<!--<div class="modal-header">-->
<!--<h2 class="modal-title colored" id="lead-programmer">LEAD PROGRAMMER</h2>-->
<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--<span aria-hidden="true">&times;</span>-->
<!--</button>-->
<!--</div>-->
<!--<div class="modal-body">-->
<!--<h3>THE IDEAL CANDIDATE:</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt, nisl-->
<!--non mattis sollicitudin, risus quam tempor sem, vel interdum est libero non-->
<!--odio.</p>-->
<!--<ul class="skill-list">-->
<!--<li>C++</li>-->
<!--<li>Dream Weaver</li>-->
<!--<li>Unreal Engine</li>-->
<!--<li>Blender</li>-->
<!--<li>Scale Form</li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>REQUIREMENTS:</h3>-->
<!--<ul>-->
<!--<li><p>B.Sc. in Computer Science and/or Mathematics</p></li>-->
<!--<li><p>Proficient with c++ and object-oriented programming</p></li>-->
<!--<li><p>Development experience in the games industry a plus.</p></li>-->
<!--<li><p>Strong communication and organizational skills</p></li>-->
<!--<li><p>Must work well under pressure and handle multiple tasks</p></li>-->
<!--<li><p>Passion for making GREAT games</p></li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>HOW TO APPLY:</h3>-->
<!--<p>If you think you have what it takes to join our team you an apply here <a-->
<!--href="mailto:office@example.com">office@example.com</a></p>-->
<!--</div>-->
<!--<div class="modal-footer">-->
<!--<button type="button" class="button secondary" data-dismiss="modal">Close</button>-->
<!--<a href="mailto:office@example.com" class="button">Apply</a>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="job-card">-->
<!--<h3 class="colored">LEVEL DESIGNER</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
<!--<button class="button" data-toggle="modal" data-target="#modal2">View Details</button>-->
<!--</div>-->
<!--&lt;!&ndash; Modal &ndash;&gt;-->
<!--<div class="modal fade " id="modal2" tabindex="-1" role="dialog" aria-labelledby="level-designer"-->
<!--aria-hidden="true">-->
<!--<div class="modal-dialog modal-lg" role="document">-->
<!--<div class="modal-content">-->
<!--<div class="modal-header">-->
<!--<h2 class="modal-title colored" id="level-designer">LEVEL DESIGNER</h2>-->
<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--<span aria-hidden="true">&times;</span>-->
<!--</button>-->
<!--</div>-->
<!--<div class="modal-body">-->
<!--<h3>THE IDEAL CANDIDATE:</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt, nisl-->
<!--non mattis sollicitudin, risus quam tempor sem, vel interdum est libero non-->
<!--odio.</p>-->
<!--<ul class="skill-list">-->
<!--<li>C++</li>-->
<!--<li>Dream Weaver</li>-->
<!--<li>Unreal Engine</li>-->
<!--<li>Blender</li>-->
<!--<li>Scale Form</li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>REQUIREMENTS:</h3>-->
<!--<ul>-->
<!--<li><p>B.Sc. in Computer Science and/or Mathematics</p></li>-->
<!--<li><p>Proficient with c++ and object-oriented programming</p></li>-->
<!--<li><p>Development experience in the games industry a plus.</p></li>-->
<!--<li><p>Strong communication and organizational skills</p></li>-->
<!--<li><p>Must work well under pressure and handle multiple tasks</p></li>-->
<!--<li><p>Passion for making GREAT games</p></li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>HOW TO APPLY:</h3>-->
<!--<p>If you think you have what it takes to join our team you an apply here <a-->
<!--href="mailto:office@example.com">office@example.com</a></p>-->
<!--</div>-->
<!--<div class="modal-footer">-->
<!--<button type="button" class="button secondary" data-dismiss="modal">Close</button>-->
<!--<a href="mailto:office@example.com" class="button">Apply</a>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="job-card">-->
<!--<h3 class="colored">AI ENGINEER</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
<!--<button class="button" data-toggle="modal" data-target="#modal3">View Details</button>-->
<!--</div>-->
<!--&lt;!&ndash; Modal &ndash;&gt;-->
<!--<div class="modal fade " id="modal3" tabindex="-1" role="dialog" aria-labelledby="ai-engineer"-->
<!--aria-hidden="true">-->
<!--<div class="modal-dialog modal-lg" role="document">-->
<!--<div class="modal-content">-->
<!--<div class="modal-header">-->
<!--<h2 class="modal-title colored" id="ai-engineer">AI ENGINEER</h2>-->
<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--<span aria-hidden="true">&times;</span>-->
<!--</button>-->
<!--</div>-->
<!--<div class="modal-body">-->
<!--<h3>THE IDEAL CANDIDATE:</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tincidunt, nisl-->
<!--non mattis sollicitudin, risus quam tempor sem, vel interdum est libero non-->
<!--odio.</p>-->
<!--<ul class="skill-list">-->
<!--<li>C++</li>-->
<!--<li>Dream Weaver</li>-->
<!--<li>Unreal Engine</li>-->
<!--<li>Blender</li>-->
<!--<li>Scale Form</li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>REQUIREMENTS:</h3>-->
<!--<ul>-->
<!--<li><p>B.Sc. in Computer Science and/or Mathematics</p></li>-->
<!--<li><p>Proficient with c++ and object-oriented programming</p></li>-->
<!--<li><p>Development experience in the games industry a plus.</p></li>-->
<!--<li><p>Strong communication and organizational skills</p></li>-->
<!--<li><p>Must work well under pressure and handle multiple tasks</p></li>-->
<!--<li><p>Passion for making GREAT games</p></li>-->
<!--</ul>-->
<!--<br>-->
<!--<h3>HOW TO APPLY:</h3>-->
<!--<p>If you think you have what it takes to join our team you an apply here <a-->
<!--href="mailto:office@example.com">office@example.com</a></p>-->
<!--</div>-->
<!--<div class="modal-footer">-->
<!--<button type="button" class="button secondary" data-dismiss="modal">Close</button>-->
<!--<a href="mailto:office@example.com" class="button">Apply</a>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="row">-->
<!--<div class="col-md-12 text-center">-->
<!--<h2 class="short-hr-center">OUR NEWSLETTER</h2>-->
<!--<p>Stay up to date with the team and our products by subscribing to our newsletter.</p>-->
<!--<form id="newsletter" data-toggle="validator">-->
<!--<input type="email" id="emailsign" placeholder="Your email adress"> &lt;!&ndash; Email Field &ndash;&gt;-->
<!--<button type="submit" id="form-signup" class="button">SUBSCRIBE</button>-->
<!--<div id="msgSignup" class="h3 text-center hidden"></div>-->
<!--</form>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!-- /// CONTACT SECTION /// -->
<!--<div id="contact" class="large-margin">-->
<!--<a href="contact.html"></a>&lt;!&ndash; Nav Anchor &ndash;&gt;-->
<!--<div class="row heading tiny-margin">-->
<!--<div class="col-md-auto">-->
<!--<h1 class="animation-element slide-down">GET IN <span class="colored">TOUCH</span></h1>-->
<!--</div>-->
<!--<div class="col">-->
<!--<hr class="animation-element extend">-->
<!--</div>-->
<!--</div>-->
<!--<div class="">-->
<!--<div class="row small-margin">-->
<!--<div class="col-md-11">-->
<!--<p>We would love to hear from you. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer-->
<!--viverra laoreet dolor sit amet blandit. Ut suscipit nisl ut risus volutpat malesuada.</p>-->
<!--</div>-->
<!--</div>-->
<!--<div class="row">-->
<!--<div class="col-md-6">-->
<!--<h2 class="short-hr-left">LEAVE US A MESSAGE</h2>-->
<!--<form id="contactForm" data-toggle="validator">-->
<!--<div class="form-group">-->
<!--&lt;!&ndash; Name Field &ndash;&gt;-->
<!--<input type="text" id="name" placeholder="Name*" required size="35"-->
<!--data-error="Name is required">-->
<!--<div class="help-block with-errors"></div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--&lt;!&ndash; Email Field &ndash;&gt;-->
<!--<input type="email" id="email" placeholder="Email*" required size="35"-->
<!--data-error="Email is required">-->
<!--<div class="help-block with-errors"></div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--&lt;!&ndash; Message Field &ndash;&gt;-->
<!--<textarea id="message" name="message" placeholder="Message*" required-->
<!--data-error="Message cannot be empty"></textarea>-->
<!--<p class="subtle">* required field</p>-->
<!--<div class="help-block with-errors"></div>-->
<!--&lt;!&ndash; Submit Button &ndash;&gt;-->
<!--<button type="submit" class="button">SEND MESSAGE</button>-->
<!--&lt;!&ndash; Success Message &ndash;&gt;-->
<!--<div id="msgSubmit" class="text-center hidden"></div>-->
<!--</div>-->
<!--</form>-->
<!--</div>-->
<!--<div class="col-md-6">-->
<!--<h2 class="short-hr-left">OUR DETAILS</h2>-->
<!--<div id="contact-info">-->
<!--<ul>-->
<!--<li><i class="fa fa-phone"></i>-->
<!--<p>Phone: <span class="colored"><a href="tel:+1(803)635585">+1 (803) 635 585</a></span>-->
<!--</p></li>-->
<!--<li><i class="fa fa-envelope"></i>-->
<!--<p>Email: <span class="colored"><a-->
<!--href="mailto:office@example.com">office@example.com</a></span></p></li>-->
<!--<li><i class="fa fa-globe"></i>-->
<!--<p>Website: <span class="colored"><a href="www.atypicalthemes.html" target="_blank">www.atypicalthemes.com</a></span>-->
<!--</p></li>-->
<!--<li><i class="fa fa-map-marker"></i>-->
<!--<p>Address: <span class="colored">1168 12th Street East Oconomowoc, WI 53066</span></p>-->
<!--</li>-->
<!--</ul>-->
<!--</div>-->
<!--&lt;!&ndash; Google Map &ndash;&gt;-->
<!--<div id="map-canvas"></div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->