<!-- /// HERO SECTION /// -->
<div id="hero-section" class="small-margin">
    <div class="row hero-unit">
        <div class="col-xl-5 col-md-12">
            <div class="hero-caption"><!-- Main Tagline -->
                <h1>THE <br> <span id="moving-text" class="colored">AWESOME, BEAUTIFUL, EPIC</span> <br> GAME <br>
                </h1>
            </div>
        </div>
        <div class="col-xl-7 col-md-12">
            <div id="hero-slider" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <img src="images/portada.png" class="d-block w-100" alt="img">
                    </div>
                    {{--<div class="carousel-item">--}}
                        {{--<img src="images/Capturas/sreyos-sol.png" class="d-block w-100" alt="img">--}}
                    {{--</div>--}}

                </div>
            </div>
        </div>
    </div>
</div><!-- Hero Section End -->